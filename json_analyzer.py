import json
from collections import OrderedDict


def tool_key_val(value, skeleton):
    typ = type(value).__name__
    skeleton.update({'type': typ})
    if isinstance(value, dict) or isinstance(value, list):
        skeleton.update({'len': len(value)})
    elif isinstance(value, str) or isinstance(value, int):
        # skeleton.update({'val': value})
        pass

    if not isinstance(value, dict):
        return None

    skeleton['dict_content'] = {}
    for key, val in value.iteritems():
        skeleton['dict_content'][str(key)] = {}
        tool_key_val(val, skeleton['dict_content'][str(key)])


def build_skeleton(j_content, suffix):
    skeleton = {}
    for key, val in j_content.iteritems():
        skeleton[str(key)] = {}
        tool_key_val(val, skeleton[key])

    od = OrderedDict(sorted(skeleton.items()))
    with open('skeleton_{}.json'.format(suffix), 'w') as fp:
        json.dump(od, fp)

    print 'Saved to skeleton_{}.json.\nReformat the file before compare'.format(suffix)


def build_skeleton_from_data_file(file_path, index):
    print 'Building skeleton for {}'.format(file_path)
    with open(file_path) as fp:
        j_cont = json.load(fp)

    build_skeleton(j_cont, index)


build_skeleton_from_data_file('new_config_dump_example.json', 'ref')
build_skeleton_from_data_file('/var/tmp/panels_config/config_dump_24_dest.json', 'my')
