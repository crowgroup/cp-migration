import json


class MigrationBase(object):
    def __init__(self):
        self.prereq = MigrationBase.load_prerequisites()

    @staticmethod
    def load_prerequisites():
        with open('prerequisites.json') as fp:
            return json.load(fp)
