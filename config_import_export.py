import os
import json
import copy
from shutil import copyfile

# from url_func import panels_panel_config_export
# from url_func import panels_panel_config_import
#
# from url_func import config_mode_post
# from url_func import config_mode_delete

from common_stuff import MigrationBase
from url_func import MigrationURL

# TODO: move to config JSON
# DIR = r'/var/tmp/panels_config'
# FILE = 'config_dump'
# PANEL_ID = 24


class MigrationPanelConfig(MigrationBase):
    def __init__(self):
        super(MigrationPanelConfig, self).__init__()

        self.url_func = MigrationURL()
        self.file_name_base = '{}_{}'.format(self.prereq['file'], self.prereq['panel_id'])
        self.file_ext = '.json'
        self.file_path = os.path.join(self.prereq['dir'], self.file_name_base + self.file_ext)

        self.src_config = {}
        self.dest_config = {}

        self.zones_indexes = {
            'src': {
                'rf': (0, self.prereq['src']['zones_rf']),
                'dect': (self.prereq['src']['zones_rf'], self.prereq['src']['zones_rf'] + self.prereq['src']['zones_dect'])
            },
            'dst': {
                'rf': (0, self.prereq['dst']['zones_rf']),
                'dect': (self.prereq['dst']['zones_rf'], self.prereq['dst']['zones_rf'] + self.prereq['dst']['zones_dect'])
            }
        }
        print 'Zones indexes:', self.zones_indexes

        self.outputs_indexes = {
            'src': {
                'rf': (0, self.prereq['src']['outputs_rf']),
                'dect': (self.prereq['src']['outputs_rf'], self.prereq['src']['outputs_rf'] + self.prereq['src']['outputs_dect'])
            },
            'dst': {
                'rf': (0, self.prereq['dst']['outputs_rf']),
                'dect': (self.prereq['dst']['outputs_rf'], self.prereq['dst']['outputs_rf'] + self.prereq['dst']['outputs_dect'])
            }
        }
        print 'Outputs indexes:', self.outputs_indexes

    # @staticmethod
    # def load_prerequisites():
    #     with open('prerequisites.json') as fp:
    #         return json.load(fp)

    def save_src_config(self):
        self.url_func.config_mode_post()
        j_cont = self.url_func.panels_panel_config_export()

        # Save src dump to json
        if not os.path.exists(self.prereq['dir']):
            os.makedirs(self.prereq['dir'])

        file_path_bkp = os.path.join(self.prereq['dir'], self.file_name_base + self.file_ext)
        idx = 0
        need_backup = False
        while os.path.exists(file_path_bkp):
            need_backup = True
            idx += 1
            file_path_bkp = os.path.join(self.prereq['dir'], self.file_name_base + '_{}'.format(idx) + self.file_ext)

        if need_backup:
            copyfile(self.file_path, file_path_bkp)
            print 'Backup ' + file_path_bkp

        with open(self.file_path, 'w') as fd:
            json.dump(j_cont, fd)

        print 'Src dump saved to {}'.format(self.file_path)

    def save_dest_config(self):
        file_path_dest = os.path.join(self.prereq['dir'], self.file_name_base + '_dest' + self.file_ext)
        with open(file_path_dest, 'w') as fd:
            json.dump(self.dest_config, fd)

        print 'Dest dump saved to {}'.format(file_path_dest)

    def read_config_from_file(self):
        with open(self.file_path) as fd:
            self.src_config = json.load(fd)

        print 'Config loaded'

    def reconfig_data(self):
        print 'Re-config is starting...'
        self.dest_config = copy.copy(self.src_config)

        keys_name_def_val = {}
        keys_name_def_val.update({'radio_zones_list': 0})           # zones serial numbers
        keys_name_def_val.update({'manually_bypassed_zone': True})
        keys_name_def_val.update({'zone_sends_reports': True})
        keys_name_def_val.update({'two_trigger_zone': False})
        keys_name_def_val.update({'stay_mode_zone': True})
        keys_name_def_val.update({'auto_bypassed_zone': False})
        keys_name_def_val.update({'zone_ipud': {"ipud": "0000000000", "unit_id": 0}})
        keys_name_def_val.update({'zone_re_trigger_count': 0})
        keys_name_def_val.update({'exit_terminator_zone': False})
        keys_name_def_val.update({'zone_will_not_report_24_hour_alarm': False})
        keys_name_def_val.update({'alt_flags_zone_link': 1})
        keys_name_def_val.update({'zone_is_on_soak_test': False})
        keys_name_def_val.update({'low_security_zone': False})
        keys_name_def_val.update({'low_security_zone': False})
        keys_name_def_val.update({'zone_is_active': False})
        keys_name_def_val.update({'zone_in_bypass_group': False})
        keys_name_def_val.update({'exit_delay_zone': True})
        keys_name_def_val.update({'armed_zone_entry_delay_time': 0})
        keys_name_def_val.update({'handover_zone': False})
        self.reconfig_zones_simple_list(keys_name_def_val)

        keys_name_def_val = {}
        keys_name_def_val.update({'zone_name': 'Zone {}'})
        self.reconfig_zones_list_indexed_val(keys_name_def_val)

        keys_name_def_val = {}
        keys_name_def_val.update({'output_pulse_time': 0})
        keys_name_def_val.update({'radio_outputs_list': 0})
        keys_name_def_val.update({'sensor_watch_to_output': False})
        keys_name_def_val.update({'temporary_output_disable': False})
        keys_name_def_val.update({'walk_test_pulse_to_output': False})
        keys_name_def_val.update({'radio_zone_supervized_fail_to_output': False})
        keys_name_def_val.update({'output_chime_time': 0})
        keys_name_def_val.update({'output_ipud': {"ipud": "0000000000", "unit_id": 0}})
        keys_name_def_val.update({'output_tamper_alarm_to_output': False})
        keys_name_def_val.update({'alt_flags_output_type': 1})
        keys_name_def_val.update({'communication_fail_to_output': False})
        keys_name_def_val.update({'output_delay_time': 0})
        keys_name_def_val.update({'fuse_fail_to_output': False})
        keys_name_def_val.update({'output_reset_time': 0})
        keys_name_def_val.update({'batt_low_to_output': False})
        keys_name_def_val.update({'system_tamper_to_output': False})
        keys_name_def_val.update({'duress_alarm_to_output': False})
        keys_name_def_val.update({'mains_fail_to_output': False})
        keys_name_def_val.update({'lockout_output': False})
        keys_name_def_val.update({'output_disable_during_disarm': False})
        keys_name_def_val.update({'monitor_output_fail_to_output': False})
        keys_name_def_val.update({'enable_output_monitoring': False})
        keys_name_def_val.update({'invert_output': False})
        keys_name_def_val.update({'pulse_output_on_kiss_off_following_arming': False})
        self.reconfig_outputs_simple_list(keys_name_def_val)

        keys_name_def_val = {}
        keys_name_def_val.update({'output_name': 'Output {}'})
        self.reconfig_outputs_list_indexed_val(keys_name_def_val)

        keys_name_def_val = {}
        keys_name_def_val.update({'keypad_tamper_alarm_to_output': '0'})
        keys_name_def_val.update({'keypad_panic_alarm_to_output': '0'})
        keys_name_def_val.update({'keypad_is_linked_to_output': '0'})
        keys_name_def_val.update({'wrong_code_alarm_to_output': '0'})
        keys_name_def_val.update({'keypad_fire_alarm_to_output': '0'})
        keys_name_def_val.update({'time_zone_assigned_to_output': '0'})
        keys_name_def_val.update({'keypad_medical_alarm_to_output': '0'})
        keys_name_def_val.update({'stay_arm_indication_to_output': [False]*self.prereq['areas_number']})
        keys_name_def_val.update({'user_code_turns_output_off': [False]*self.prereq['users_number']})
        keys_name_def_val.update({'pendant_fire_alarm_to_output': [False]*self.prereq['users_number']})
        keys_name_def_val.update({'armed_exit_delay_beeps_to_output': [False]*self.prereq['areas_number']})
        keys_name_def_val.update({'pendant_panic_alarm_to_output': [False]*self.prereq['users_number']})
        keys_name_def_val.update({'stay_exit_delay_beeps_to_output': [False]*self.prereq['areas_number']})
        keys_name_def_val.update({'pendant_tag_stay_disarm_beep_to_output': [False]*self.prereq['areas_number']})
        keys_name_def_val.update({'arm_beeps_to_output': [False]*self.prereq['areas_number']})
        keys_name_def_val.update({'disarm_beeps_to_output': [False]*self.prereq['areas_number']})
        keys_name_def_val.update({'pendant_tag_disarm_beep_to_output': [False]*self.prereq['areas_number']})
        keys_name_def_val.update({'pendant_tag_stay_arm_beep_to_output': [False]*self.prereq['areas_number']})
        keys_name_def_val.update({'stay_disarm_beeps_to_output': [False]*self.prereq['areas_number']})
        keys_name_def_val.update({'disarm_indication_to_output': [False]*self.prereq['areas_number']})
        keys_name_def_val.update({'pendant_tag_arm_beep_to_output': [False]*self.prereq['areas_number']})
        keys_name_def_val.update({'stay_arm_beeps_to_output': [False]*self.prereq['areas_number']})
        keys_name_def_val.update({'pendant_medical_alarm_to_output': [False]*self.prereq['users_number']})
        keys_name_def_val.update({'arm_indication_to_output': [False]*self.prereq['areas_number']})
        keys_name_def_val.update({'pulse_output_every_5_sec_when_disarmed': [False]*self.prereq['areas_number']})
        keys_name_def_val.update({'user_code_turns_output_on': [False]*self.prereq['users_number']})
        self.reconfig_outputs_simple_dictionary(keys_name_def_val)

        keys_name_def_val = {}
        keys_name_def_val.update({'zone_near_alarm_assigned_to_output': [False]*self.zones_indexes['dst']['dect'][1]})
        keys_name_def_val.update({'zone_tamper_assigned_to_output': [False]*self.zones_indexes['dst']['dect'][1]})
        keys_name_def_val.update({'chime_zone_alarm_assigned_to_output': [False]*self.zones_indexes['dst']['dect'][1]})
        keys_name_def_val.update({'zone_24_hour_alarm_assigned_to_output': [False]*self.zones_indexes['dst']['dect'][1]})
        keys_name_def_val.update({'stay_mode_entry_delay_assigned_to_output': [False]*self.zones_indexes['dst']['dect'][1]})
        keys_name_def_val.update({'zone_stay_alarm_assigned_to_output': [False]*self.zones_indexes['dst']['dect'][1]})
        keys_name_def_val.update({'zone_alarm_assigned_to_output': [False]*self.zones_indexes['dst']['dect'][1]})
        keys_name_def_val.update({'armed_zone_entry_delay_assigned_to_output': [False]*self.zones_indexes['dst']['dect'][1]})
        keys_name_def_val.update({'zone_verified_alarm_assigned_to_output': [False]*self.zones_indexes['dst']['dect'][1]})
        self.reconfig_outputs_dictionary_with_zones_list(keys_name_def_val)

        self.save_dest_config()

    def update_panel_config(self):
        self.url_func.config_mode_post()
        self.url_func.panels_panel_config_import(self.dest_config)
        self.url_func.config_mode_delete()

    def _reformat_zones_simple_list(self, src_lst, def_val):
            # Copy RF values from src to dst
            dest_lst = src_lst[self.zones_indexes['src']['rf'][0]:self.zones_indexes['src']['rf'][1]]

            # Fill "extra" RF values by it default and concatenate to destination list
            dest_lst += [def_val] * (self.zones_indexes['dst']['rf'][1] - len(dest_lst))

            # Copy DECT values from src to dst and concatenate to destination list
            dest_lst += src_lst[self.zones_indexes['src']['dect'][0]:self.zones_indexes['src']['dect'][1]]

            # Fill "extra" DECT values by it default and concatenate to destination list
            dest_lst += [def_val] * (self.zones_indexes['dst']['dect'][1] - len(dest_lst))
            # print 'DST list:', dest_lst, 'len:', len(dest_lst)
            return dest_lst

    def reconfig_zones_simple_list(self, keys_name_def_val):
        for key, def_val in keys_name_def_val.iteritems():
            dest_lst = self._reformat_zones_simple_list(self.src_config[key], def_val)
            self.dest_config.update({key: dest_lst})

    def reconfig_zones_list_indexed_val(self, keys_name_def_val):  # def_val like 'Zone {}'
        for key, def_val in keys_name_def_val.iteritems():
            # Copy RF values from src to dest
            dest_lst = self.src_config[key][self.zones_indexes['src']['rf'][0]:self.zones_indexes['src']['rf'][1]]

            # Fill all "extra" values by indexed default like 'Zone <idx>'
            for idx in xrange(self.zones_indexes['src']['rf'][1], self.zones_indexes['dst']['dect'][1]):
                dest_lst.append(def_val.format(idx+1))

            # Rename defaults from relevant src DECT
            for idx in xrange(self.zones_indexes['src']['dect'][0], self.zones_indexes['src']['dect'][1]):
                if not def_val.format(idx+1) in str(self.src_config[key][idx]):
                    dest_lst[idx + (self.zones_indexes['dst']['dect'][0] - self.zones_indexes['src']['dect'][0])] =  \
                        self.src_config[key][idx]

            self.dest_config.update({key: dest_lst})

    def reconfig_outputs_simple_list(self, keys_name_def_val):
        for key, def_val in keys_name_def_val.iteritems():
            # Copy RF values from src to dst
            dest_lst = self.src_config[key][self.outputs_indexes['src']['rf'][0]:self.outputs_indexes['src']['rf'][1]]

            # Fill "extra" RF values by it default and concatenate to dest list
            dest_lst += [def_val] * (self.outputs_indexes['dst']['rf'][1] - len(dest_lst))

            # Copy DECT values from src to dst and concatenate to dest list
            dest_lst += self.src_config[key][self.outputs_indexes['src']['dect'][0]:self.outputs_indexes['src']['dect'][1]]

            # Fill "extra" DECT values by it default and concatenate to dest list
            dest_lst += [def_val] * (self.outputs_indexes['dst']['dect'][1] - len(dest_lst))

            self.dest_config.update({key: dest_lst})

    def reconfig_outputs_list_indexed_val(self, keys_name_def_val):  # def_val like 'Output {}'
        for key, def_val in keys_name_def_val.iteritems():
            # Copy RF values from src to dest
            dest_lst = self.src_config[key][self.outputs_indexes['src']['rf'][0]:self.outputs_indexes['src']['rf'][1]]

            # Fill all "extra" values by indexed default like 'Output <idx>'
            for idx in xrange(self.outputs_indexes['src']['rf'][1], self.outputs_indexes['dst']['dect'][1]):
                dest_lst.append(def_val.format(idx+1))

            # Rename defaults from relevant src DECT
            for idx in xrange(self.outputs_indexes['src']['dect'][0], self.outputs_indexes['src']['dect'][1]):
                if not def_val.format(idx+1) in str(self.src_config[key][idx]):
                    dest_lst[idx + (self.outputs_indexes['dst']['dect'][0] - self.outputs_indexes['src']['dect'][0])] =  \
                        self.src_config[key][idx]

            self.dest_config.update({key: dest_lst})

    def reconfig_outputs_simple_dictionary(self, keys_name_def_val):
        for key, def_val in keys_name_def_val.iteritems():
            # dest_dic = copy.copy(self.dest_config[key])
            dest_dic = copy.copy(self.src_config[key])

            # Fill all "extra" values by default
            for idx in xrange(self.outputs_indexes['src']['rf'][1], self.outputs_indexes['dst']['dect'][1]):
                dest_dic[str(idx)] = def_val

            # Copy DECT values from src to dst
            for idx in xrange(self.outputs_indexes['src']['dect'][0], self.outputs_indexes['src']['dect'][1]):
                dest_dic[str(idx + (self.outputs_indexes['dst']['dect'][0] - self.outputs_indexes['src']['dect'][0]))] = \
                    self.src_config[key][str(idx)]

            self.dest_config.update({key: dest_dic})

    def reconfig_outputs_dictionary_with_zones_list(self, keys_name_def_val):  # def_val like [False]*<dst zones number>
        for key, def_val in keys_name_def_val.iteritems():
            dest_dic = copy.copy(self.src_config[key])

            # Reformat value of RF outputs
            for idx in xrange(self.outputs_indexes['src']['rf'][0], self.outputs_indexes['src']['rf'][1]):
                dest_dic[str(idx)] = self._reformat_zones_simple_list(dest_dic[str(idx)], def_val[0])

            # Fill all "extra" values by default
            for idx in xrange(self.outputs_indexes['src']['rf'][1], self.outputs_indexes['dst']['dect'][1]):
                dest_dic[str(idx)] = def_val

            # Copy DECT values from src to dst
            for idx in xrange(self.outputs_indexes['src']['dect'][0], self.outputs_indexes['src']['dect'][1]):
                dest_dic[str(idx + (self.outputs_indexes['dst']['dect'][0] - self.outputs_indexes['src']['dect'][0]))] = \
                    self._reformat_zones_simple_list(self.src_config[key][str(idx)], def_val[0])

            self.dest_config.update({key: dest_dic})


# def read_json_example():
#     file_name = r'/var/tmp/panels_config/manually.json'
#     with open(file_name) as fd:
#         j_cont = json.load(fd)
#
#     print 'Read:\n', j_cont
#


def main():
    migration = MigrationPanelConfig()
    migration.save_src_config()
    migration.read_config_from_file()
    migration.reconfig_data()
    # migration.update_panel_config()

if __name__ == '__main__':
    main()