
import sys
import base64
import requests
from requests.exceptions import HTTPError

# from common_stuff import PANEL_ID
# from common_stuff import common_check
# from login import TOKEN
# from login import URL_BASE

from common_stuff import MigrationBase


class MigrationURL(MigrationBase):
    def __init__(self):
        # MigrationBase.__init__(self)
        super(MigrationURL, self).__init__()
        self.token = self.login_get_token()

    @staticmethod
    def common_check(resp):
        status_code = resp.status_code
        print 'status code:', status_code
        try:
            resp.raise_for_status()
        except HTTPError, e:
            print e
            assert False

        assert status_code == 200

        if 'json' not in resp.headers['content-type']:
            print 'Non-json response'
            assert False

        return True

    def login_get_token(self):
        encoded = base64.urlsafe_b64encode(self.prereq['authorization']['client_id'] + ':' +
                                           self.prereq['authorization']['secret'])
        login = {
            'username': 'Igorg@crow.co.il',
            'password': 'igor123',
            'grant_type': 'password'
        }
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + encoded
        }

        resp = requests.post(self.prereq['url_base'] + "/o/token/", data=login, headers=headers)
        result = resp.json()
        print 'Authorization result: ', result
        token = result['access_token']
        return token

    def panels_panel_config_export(self):
        print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(self.prereq['panel_id'])
        url = self.prereq['url_base'] + '/panels/{}/config_export_import/'.format(self.prereq['panel_id'])
        resp = requests.get(url, headers={'Authorization': 'Bearer ' + self.token})
        rc = MigrationURL.common_check(resp)
        assert rc is True

        j_cont = resp.json()
        return j_cont

    def panels_panel_config_import(self, config_data):
        print '\n[' + sys._getframe().f_code.co_name + ' on panel {}]'.format(self.prereq['panel_id'])
        url = self.prereq['url_base'] + '/panels/{}/config_export_import/'.format(self.prereq['panel_id'])
        resp = requests.put(url, headers={'Authorization': 'Bearer ' + self.token}, json=config_data)
        rc = self.common_check(resp)
        assert rc is True

        j_cont = resp.json()
        print 'Zones: ', j_cont["zone_name"]
        print 'Outputs: ', j_cont["output_name"]

    def config_mode_post(self):
        print '\n[' + sys._getframe().f_code.co_name + ']'
        url = self.prereq['url_base'] + '/panels/{}/config_mode/'.format(self.prereq['panel_id'])
        config_data = {}
        resp = requests.post(url, headers={'Authorization': 'Bearer ' + self.token, 'X-Crow-CP-installer': '000000'},
                             data=config_data)
        rc = self.common_check(resp)
        assert rc is True

        j_cont = resp.json()
        try:
            print 'config: ', j_cont['config']
        except KeyError, e:
            print e
            rc = False
        finally:
            assert rc is True

    def config_mode_delete(self):
        print '\n[' + sys._getframe().f_code.co_name + ']'
        url = self.prereq['url_base'] + '/panels/{}/config_mode/'.format(self.prereq['panel_id'])
        config_data = {}
        resp = requests.delete(url, headers={'Authorization': 'Bearer ' + self.token}, data=config_data)
        rc = self.common_check(resp)
        assert rc is True

        j_con = resp.json()
        try:
            print 'config: ', j_con['config']
        except KeyError, e:
            print e
            rc = False
        finally:
            assert rc is True
